#!/bin/bash

if [ $# -eq 0 ]; then
	rowCount=100
else
	rowCount=$1
fi

for ((i=0; i<$rowCount; i+=5 )); do
	data1='{name="Bob '$i'", address="Georgi Ring '$i'"}'
	data2='{name="Bob '$((i+1))'", address="Haselweg '$i'"}'
	data3='{name="Bob '$((i+2))'", address="Auf dem Berge '$i'"}'
	data4='{name="Bob '$((i+3))'", address="Hansastraße '$i'"}'
	data5='{name="Bob '$((i+4))'", address="Kloppstockstraße '$i'"}'
#	echo "$data1"
	curl -X POST -H "Content-Type: application/x-www-form-urlencoded" -d "$data1" http://localhost:8080/rest/person &
	curl -X POST -H "Content-Type: application/x-www-form-urlencoded" -d "$data2" http://localhost:8080/rest/person &
	curl -X POST -H "Content-Type: application/x-www-form-urlencoded" -d "$data3" http://localhost:8080/rest/person &
	curl -X POST -H "Content-Type: application/x-www-form-urlencoded" -d "$data4" http://localhost:8080/rest/person &
	curl -X POST -H "Content-Type: application/x-www-form-urlencoded" -d "$data5" http://localhost:8080/rest/person &
done
