package de.oftik.kotson

import java.nio.charset.Charset

import de.oftik.kotson.dao.PersonDAO
import de.oftik.kotson.undertow.Exchange

import io.undertow.util.StatusCodes
import io.undertow.server.HttpHandler
import io.undertow.server.HttpServerExchange
import io.undertow.util.Headers
import com.google.gson.Gson
import com.google.gson.GsonBuilder

// Definition of a variety of handlers illustrating different concepts, e.g. single DTO result, list of DTOs and parsing body.

class AllPersons : HttpHandler {
	// a private and immutable attribute => no setter
	private val personDao: PersonDAO

	// explicit constructor
	public constructor(dao: PersonDAO) {
		this.personDao = dao
	}

	override fun handleRequest(exchange: HttpServerExchange) {
		exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json")
		val gson = Gson()
		exchange.getResponseSender().send(gson.toJson(personDao.findAll()));
	}
}

// immutable attribute and implicit constructor
class FindPerson(val personDao: PersonDAO) : HttpHandler {
	override fun handleRequest(exchange: HttpServerExchange) {
		exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json")
		val idParam = Exchange.pathParam(exchange.getQueryParameters(), "id");
		if (!idParam.isPresent()) {
			exchange.setStatusCode(StatusCodes.NOT_FOUND)
			return;
		}
		val gson = Gson()
		exchange.getResponseSender().send(gson.toJson(personDao.findById(idParam.get())));
	}
}

class CreatePerson(private val personDao: PersonDAO) : HttpHandler {
	override fun handleRequest(exchange: HttpServerExchange) {
		exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json")
		val gson = Gson()
		val np = gson.fromJson(readBody(exchange), KPerson::class.java);
		personDao.persist(np);
		exchange.setStatusCode(StatusCodes.NO_CONTENT)
	}

	// such sophisticated NIO tricks ought to be hidden in pure Java code, nevertheless: works
	// in Kotlin, too
	fun readBody(exchange: HttpServerExchange): String {
		val pooledByteBuffer = exchange.getConnection().getByteBufferPool().allocate();
		val byteBuffer = pooledByteBuffer.getBuffer();

		byteBuffer.clear();

		val requestChannel = exchange.getRequestChannel();
		requestChannel.read(byteBuffer);
		val pos = byteBuffer.position();
		byteBuffer.rewind();
		val bytes = ByteArray(pos);
		byteBuffer.get(bytes);

		val requestBody = String(bytes, Charset.forName("UTF-8"));

		byteBuffer.clear();
		pooledByteBuffer.close();
		return requestBody;
	}

}