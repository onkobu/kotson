package de.oftik.kotson

import de.oftik.kotson.dao.PersonDAO;

import io.undertow.util.StatusCodes;
import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.RoutingHandler;
import io.undertow.util.Headers


// Strapping all files/ classes/ dependencies together
fun main(args: Array<String>) {
	println("works")

	val personDao = PersonDAO();

	personDao.persist(KPerson("Enrico", "Haselweg 3"))
	personDao.persist(KPerson("Beate", "Pfaffenstieg 17b"))
	personDao.persist(KPerson("Hulk", "S.H.I.E.L.D."))

	// A singleton/ anonymous instance
	val handler = object : HttpHandler {
		override fun handleRequest(exchange: HttpServerExchange) {
			exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/plain")
			exchange.getResponseSender().send("Hello World")
		}
	}

	val server = Undertow.builder()
			.addHttpListener(8080, "localhost")
			.setHandler(Handlers.path()
					.addPrefixPath("hello", handler)
					.addPrefixPath("/rest", RoutingHandler()
							.get("/person", AllPersons(personDao))
							.get("/person/{id}", FindPerson(personDao))
							.post("/person", CreatePerson(personDao)))
			).build()
	server.start()
}
