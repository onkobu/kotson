package de.oftik.kotson

import java.util.UUID

import de.oftik.kotson.dao.Person

/**
 * Realizes an external interface and acts as data transfer object (DTO). Its ID attribute is immutable.
 */
class KPerson(// var is mutable, val is immutable
		private var name: String = "",
		private var address: String = ""
) : Person {
	private val id: String = UUID.randomUUID().toString()

	override fun getAddress(): String = address

	override fun getName() = name

	override fun getId() = id

	override fun merge(other: Person) {
		if (this === other || id != other.id) {
			return
		}

		if (name != other.name) {
			name = other.name
		}

		if (address != other.address) {
			address = other.address
		}
	}
}