package de.oftik.kotson.dao;

public interface Person {
	String getId();

	String getName();

	String getAddress();

	void merge(Person other);
}
