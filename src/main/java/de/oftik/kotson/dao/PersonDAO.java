package de.oftik.kotson.dao;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class PersonDAO {
	private final Map<String, Person> persons = new ConcurrentHashMap<>();

	public Person persist(Person p) {
		final Person person = persons.putIfAbsent(p.getId(), p);
		if (person == p) {
			return person;
		}
		if (person == null) {
			return p;
		}
		person.merge(p);
		return person;
	}

	public Person findById(String id) {
		return persons.get(id);
	}

	public Collection<Person> findAll() {
		return persons.values();
	}
}
