package de.oftik.kotson.undertow;

import java.util.Deque;
import java.util.Map;
import java.util.Optional;

import io.undertow.server.HttpServerExchange;

public class Exchange {
	public static Optional<String> pathParam(HttpServerExchange exc, String key) {
		return pathParam(exc.getQueryParameters(), key);
	}

	public static Optional<String> pathParam(Map<String, Deque<String>> map, String key) {
		return Optional.ofNullable(map.get(key)).map(Deque::getFirst);
	}
}
