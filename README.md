# kotson

Kotlin and Java deliver JSON based on Undertow. A sample project to show the difference between Java's verbosity and Kotlin's chaos. A DAO (data access object) is implemented in Java, e.g. a library expressing its contract with (Java) interfaces. A Kotlin application implements such an interface and uses the DAO to simulate some persistence mechanism.

The simulated persistence layer could be anything from a local file or even in-memory H2 database to a cloud based storage.

# Why Kotlin and Undertow

Kotlin runs on the Java VM right away. Projects can be based on Maven and thus work in many IDEs. It strips object oriented programming down to a very basic syntax. There is much more by contract than in Java.

Undertow is one of my favorites and there are many tutorials putting Kotlin on top of Spring. Personally I don't get it, why one should write a very small amount of code being put into a huge eco system with even more contract. Undertow is slick, fast and puts network sockets at my fingertips. Why bothering auto configuration when you get path parameters as stream-capable map?

# Build and run

## Preconditions

- decent JDK, choose OpenJDK 11 (LTS) or newer
- Maven-installation from https://maven.apache.org, 3.6 or later

## Make it so

```
mvn clean install

java -jar target/<jar-file-with-dependencies>
```

## Access it

The server uses port 8080 on localhost (http://localhost:8080/):

* hello - a simple static message
* rest/person - list of all entities (GET) or insert of a new (POST)
* rest/person/{id} - a particular entity, id from #2

See also `fill.sh` for a cURL sample.

# Links

* https://itstream.de/sqlite-mit-kotlin-quichstart/ – Tutorial to bring in SQLite 

